-- para hacer el build
-- ubicarse dentro de la carpeta del proyecto y ejecutar
./mvnw package
-- para hacer deploy local ubicarse en la carpeta
cd target/
-- ejecutar el jar
java -jar spring-boot-web-0.0.1-SNAPSHOT.jar

--------------------------------------------
Recursos de Spring MVC y la Inyección de Dependencia

Descargar archivos PDFs que contienen un resumen sobre Inyección de Dependencia y Spring MVC, conceptos que iremos abarcando a lo largo del curso.

Importante leer!!! para complementar con lo que iremos viendo en las próximas clases!
Recursos de esta clase

    ppt_anotaciones_spring.pdf
    ppt_spring_web_mvc.pdf

--------------------------------------------

Descargar Código Fuente

En ésta clase puedes descargar todo el código fuente de los ejemplos desarrollados en ésta sección.
Recursos de esta clase

    spring-boot-web.zip