package pe.itcorp.spring5.web.app.controllers;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import pe.itcorp.spring5.web.app.models.Usuario;

@Controller
@RequestMapping("/app") //ruta de primer nivel
public class IndexController {
	
	@Value("${texto.indexcontroller.index.titulo}")
	private String textoIndex;
	
	@Value("${texto.indexcontroller.perfil.titulo}")
	private String textoPerfil;
	
	@Value("${texto.indexcontroller.listar.titulo}")
	private String textoListar;

	// por lo general, se usa @Requestmapping
	// al usar @GetMapping, es lo mismo que usar @Requestmapping. La diferencia esta en que @GetMapping es solo para el metodo GET 
	// por default, al usar @Requestmapping y no especificar el metodo, se asume GET
	// asimismo, se podria asociar mas de una ruta usando llaves
	// @GetMapping(value="/index") // para una sola ruta
	@GetMapping({"/index", "","/","/home"}) // para varias rutas. rutas de segundo nivel
	public String index(Model model) { // tb se puede usar el ModelMap o Map
	//public ModelAndView index(ModelAndView mv) { // tb se puede usar el ModelMap o Map
		// pasar usar ModelMap y Model
		// Model model
		model.addAttribute("titulo",textoIndex);
		return "index";
		
		// para usar map
		// Map<String, Object> map
		// map.put("titulo", "probando MAP");
		// return "index";
		
		// usando modelandview
		// mv.addObject("titulo", "probando ModelAndView");
		// mv.setViewName("index");
		// return mv;
	}
	
	@RequestMapping("/perfil")
	public String perfil(Model model) {
		
		Usuario usuario = new Usuario();
		usuario.setNombre("Carlos");
		usuario.setApellido("Coaquira");
		usuario.setEmail("correo@gmail.com");
		
		model.addAttribute("usuario", usuario);
		model.addAttribute("titulo",textoPerfil.concat(usuario.getNombre()));
		
		return "perfil";
	}
	
	@RequestMapping("/listar")
	public String listar(Model model) {
		
		
		
		model.addAttribute("titulo",textoListar);
		// model.addAttribute("usuarios", usuarios);
		
		return "listar";

	}
	
	@ModelAttribute("usuarios")
	public List<Usuario> poblarUsuarios() {
		List<Usuario> usuarios  = new ArrayList<>();
		
		usuarios.add(new Usuario("nombre01","apellido01","email01"));
		usuarios.add(new Usuario("nombre02","apellido02","email02"));
		usuarios.add(new Usuario("nombre03","apellido03","email03"));
		usuarios.add(new Usuario("nombre04","apellido04","email04"));
		
		return usuarios;
	}
}
